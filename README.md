# SchuleB2
Hallo lieber Lukas,
	 Prüfungsangst habe ich nicht so stark wie Du, aber ich habe schon von anderen gehört, dass das ein sehr großes Problem werden kann, so mit Schlafproblemen und dann ist man nicht richtig fit im Kopf und es ist wie ein Teufelskreis.

	Ein bisschen Angst vor einer Prüfung - hat mein Vater gesagt-dass allerdings gut ist, da man sich konzentriert auf diese Sache und der Körper und der Geist sich darauf einstellen können. Aber wie bei den meisten Dingen im Leben ist der goldene Mittelweg hier das Beste, allerdings kann sich das nicht jeder aussuchen. Denke bitte immer daran, dass es schlimmere Dinge im Leben gibt, als durch eine Prüfung durchzufallen. Was nützt das beste Abschlusszeugenis, wenn Du später eine schweren Verkehrsunfall hast oder Pech in der Liebe, und dann vielleicht sogar depressiv werden kannst, was viel schlimmer ist. Auch in der Schule sitzenzubleiben ist nicht so schlimm wie gedacht, manchmal brauch man für einiges mehr Zeit, und holt es später dreifach rein, wir sind alle individuelle und vielleicht macht auch alles einen Sinn, so wie es läuft.
	Mir persönlich hilft immer Ausdauersport, um Ängste und Agressionen  abzubauen, ich glaube noch besser an der frischen Luft, z.B. Joggen wird der Serotoninspiegel aufgebaut un d dann kann man besser schlafen, was auch wissenshaftlich nachgewiesen ist.  Alkohol hilft mir auch , aber bitte nur in maßen.
	Ich drücke Dir die Daumen  und treffen uns Kiel, selbst wenn Du durchfallen solltest. Vielleicht hat Gott dich für einen anderen Beruf ausgewählt. Der Mensch denkt, Gott lenkt.
	Schöne Grüße zurück auch an Isabell Yussuf



